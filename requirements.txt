docker==4.2
pytz==2018.9
bitbucket-pipes-toolkit==2.2.0
git+https://bitbucket.org/atlassian/aws-cloudformation-deploy.git@0.11.1
